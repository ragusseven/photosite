﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PhotoMvc.Models;
using PhotoStorage;

namespace PhotoMvc.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private static readonly PostManager _postManager = new PostManager();

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        ViewBag.Posts = _postManager.GetPosts()
                                    .Select(x => new PostPreviewModel(x.PostHeader, x.PostText, x.ImagePath));
        return View();
    }

    public IActionResult Privacy()
    {
        var view = View();
        return View();
    }

    public IActionResult Publish()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
    [HttpPost]
    public IActionResult Publish(string header, string text,[FromForm(Name = "uploadedFile")] IFormFile uploadedFile)
    {
        if (uploadedFile.Length <= 0) return View();
        
        var fileName = Path.GetFileName(uploadedFile.FileName);
        var filePath = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/Images", fileName);
        using (var fileStream = new FileStream(filePath, FileMode.Create))
        {
            uploadedFile.CopyTo(fileStream);
        }

        _postManager.AddPost(header, text, $"/Images/{fileName}");
        return View();
    }
}