namespace PhotoMvc.Models;

public class PostPreviewModel
{
    public PostPreviewModel(string postHeader, string postText, string imagePath)
    {
        PostHeader = postHeader;
        PostText = postText;
        ImagePath = imagePath;
    }

    public string PostHeader { get; set; }
    public string PostText { get; set; }
    public string ImagePath { get; set; }
}