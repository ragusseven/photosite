﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

/*$("textarea").each(function () {
    this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
}).on("input", function () {
    this.style.height = 0;
    this.style.height = (this.scrollHeight) + "px";
});*/

jQuery(function ($) {
    $(window).resize(function (event) {
        HeaderCalc();
    });
});

function HeaderCalc() {
    let header = document.getElementById("myHeader");
    let offset = header.offsetHeight;
    let mainFrame = document.getElementById("mainFrame");
    mainFrame.style.paddingTop = offset + 5 + "px";
}

HeaderCalc();