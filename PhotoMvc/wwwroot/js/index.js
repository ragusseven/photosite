// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
/*$("textarea").each(function () {
    this.setAttribute("style", "height:" + (this.scrollHeight) + "px;overflow-y:hidden;");
}).on("input", function () {
    this.style.height = 0;
    this.style.height = (this.scrollHeight) + "px";
});*/

jQuery(function ($) {
    $(window).resize(function (event) {
        OnResize();
    });
});

function OnResize() {
    let posts = document.querySelectorAll(".post_plate");
    for (let post of posts) {
        var content = post.firstElementChild;
        var image = post.lastElementChild;
        let height = image.offsetHeight;
        let width = image.offsetWidth;
        content.style.maxHeight = height + "px";
        content.style.maxWidth = `calc(100% - ${width})`;
    }
}

OnResize();