﻿namespace PhotoStorage;

public class PostManager
{
    private List<Post> _posts;
    public bool Empty = true;

    public PostManager()
    {
        _posts = new List<Post>();
    }

    public void AddPost(string header, string text, string imagePath)
    {
        _posts.Add(new Post(header, text, imagePath));
        Empty = false;
    }

    public List<Post> GetPosts()
    {
        return _posts;
    }
}

public class Post
{
    public Post(string postHeader, string postText, string imagePath)
    {
        PostHeader = postHeader;
        PostText = postText;
        ImagePath = imagePath;
    }

    public string PostHeader { get; set; }
    public string PostText { get; set; }
    public string ImagePath { get; set; }
}